#Author: naveenraj_b@infosys.com
Feature: As an api user I want to retrieve given number of ZeroFollowers Github User Account

  # See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As an api user I want to retrieve given number of Github User Account having ZeroFollowers

  Scenario Outline: Get <limit> github ZeroFollowers User Account
		Given url microserviceUrl
		And path '/zerofollowers/<limit>'
		When method GET
		Then status 200
		And match header Content-Type contains 'application/json'
		# see https://github.com/intuit/karate#schema-validation
		# Define the required schema
		* def githubAccountSchema = { id : '#number', login : '#string', html_url : '#string' } }
		# The response should have an array of given limit of github Accounts
		And match response == '#[<limit>] githubAccountSchema'

    Examples:
			| limit |
      | 5     |
      | 15    |
      | 1     |

      
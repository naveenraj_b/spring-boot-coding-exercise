package com.telstra.codechallenge.githubzerofollowers.entity;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GithubAccountEntity {

	private List<AccountDetails> items;
	
	public GithubAccountEntity() {
		items = new ArrayList<>();
	}

	public List<AccountDetails> getItems() {
		return items;
	}

	public void setItems(List<AccountDetails> items) {
		this.items = items;
	}

	
}

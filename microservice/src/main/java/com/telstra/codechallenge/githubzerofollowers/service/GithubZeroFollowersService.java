package com.telstra.codechallenge.githubzerofollowers.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.githubzerofollowers.entity.AccountDetails;
import com.telstra.codechallenge.githubzerofollowers.entity.GithubAccountEntity;

@Service
public class GithubZeroFollowersService {

	@Value("${github.base.url}")
	private String githubBaseUrl;

	@Autowired
	private RestTemplate restTemplate;
	
	private String gitUserSearchPath = "/search/users";
	private String noOfFollowers = "0";
	private String followerQuery ="?q=followers:"+ noOfFollowers +"&sort=joined&order=asc";

	public List<AccountDetails> getZeroFollowers(int limit) {
		GithubAccountEntity result = restTemplate.getForObject(githubBaseUrl + gitUserSearchPath + followerQuery ,
				GithubAccountEntity.class);
		return result.getItems().stream()
				.limit(limit)
				.collect(Collectors.toList());
	}
}

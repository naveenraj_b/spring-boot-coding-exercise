package com.telstra.codechallenge.githubzerofollowers.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.githubzerofollowers.entity.AccountDetails;
import com.telstra.codechallenge.githubzerofollowers.service.GithubZeroFollowersService;

@RestController
public class GithubZeroFollowersController {
	
	@Autowired
	private GithubZeroFollowersService githubZeroFollowersService;
	
	@GetMapping("/zerofollowers/{limit}")
	public ResponseEntity<List<AccountDetails>> getZeroFollowers(@PathVariable String limit) {
		List<AccountDetails> response;
		int userLimit;
		try {
			userLimit = Integer.parseInt(limit);
			response = githubZeroFollowersService.getZeroFollowers(userLimit);
		}catch (Exception e) {
			response = null;
		}
		return new ResponseEntity<List<AccountDetails>>(response,HttpStatus.OK);
	}

}

package com.telstra.codechallenge.githubzerofollowers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import com.telstra.codechallenge.githubzerofollowers.entity.AccountDetails;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MicroServiceMainTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void TestHealth() throws RestClientException, MalformedURLException {
		ResponseEntity<String> response = restTemplate
				.getForEntity(new URL("http://localhost:" + port + "/actuator/health").toString(), String.class);
		assertEquals("{\"status\":\"UP\"}", response.getBody());
	}

	@Test
	public void TestZeroFollowersPositive() throws RestClientException, MalformedURLException {
		ResponseEntity<AccountDetails[]> response = restTemplate.getForEntity(
				new URL("http://localhost:" + port + "/zerofollowers/5").toString(), AccountDetails[].class);
		//Validate Status
		assertEquals(200,response.getStatusCodeValue());
		//Validate length of response body
		assertTrue(response.getBody().length == 5);
	}
	
	@Test
	public void TestZeroFollowersNegative() throws RestClientException, MalformedURLException {
		ResponseEntity<AccountDetails[]> response = restTemplate.getForEntity(
				new URL("http://localhost:" + port + "/zerofollowers/bc").toString(), AccountDetails[].class);
		//Validate Status
		assertEquals(200,response.getStatusCodeValue());
		//Validate response body to null
		assertTrue(response.getBody() == null);
	}
}

# Spring Boot Coding Exercise

## Project Structure

This is a multi module maven project with two modules:

- The `micoservice` module produces a spring boot application.
- The `functional-tests` is used to run functional tests using the [karate](https://github.com/intuit/karate) library.


### Assesment completed for

- Find the oldest user accounts with zero followers in github

The endpoint will accept a parameter that sets the number of accounts to return.

The following fields will be returned in array:

      id
      login
      html_url

URL sample: http://localhost:8008/zerofollowers/{limit}



## Test Reports

** 1. Micoservice module **

- [Junit Report](https://bitbucket.org/naveenraj_b/spring-boot-coding-exercise/src/master/microservice/reporting/junit/)
- [code coverage](https://bitbucket.org/naveenraj_b/spring-boot-coding-exercise/src/master/microservice/reporting/code%20coverage/jacoco/)

** 2. functional-tests module **

- [Junit Report](https://bitbucket.org/naveenraj_b/spring-boot-coding-exercise/src/master/functional-tests/reporting/surefire-reports/)